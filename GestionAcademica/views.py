from django.shortcuts import render
from GestionAcademica import forms, models
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.

def index(request):
    return render(request, "index.html")

def crearAlumno(request):
    if request.method == "POST":
        formularioCrearAlumno = forms.AlumnoForm(request.POST, prefix="formularioCrearAlumno")
        if formularioCrearAlumno.is_valid():
            cedula = formularioCrearAlumno.cleaned_data.get('cedula')
            nombres = formularioCrearAlumno.cleaned_data.get('nombres')
            apellido_materno = formularioCrearAlumno.cleaned_data.get('apellido_materno')
            apellido_paterno = formularioCrearAlumno.cleaned_data.get('apellido_paterno')
            fecha_nacimiento = formularioCrearAlumno.cleaned_data.get('fecha_nacimiento')
            sexo = formularioCrearAlumno.cleaned_data.get('sexo')
            nuevoAlumno = models.Alumno(cedula=cedula, nombres=nombres, apellido_paterno=apellido_paterno, apellido_materno=apellido_materno, fecha_nacimiento=fecha_nacimiento, sexo=sexo)
            nuevoAlumno.save()
            return HttpResponseRedirect(reverse('index'))
    else:
        formularioCrearAlumno = forms.AlumnoForm(prefix="formularioCrearAlumno")
    return render(request, "formularioCrearAlumno.html", {'formularioCrearAlumno':formularioCrearAlumno})

def listarAlumnos(request):
    alumnosRegistrados = models.Alumno.objects.filter()
    return render(request, "formularioListarAlumnos.html",{'listaAlumnos':alumnosRegistrados})

def actualizarAlumno(request, id):
    instance = models.Alumno.objects.get(id=id)
    if request.method == "POST":
        formularioActualizarAlumno = forms.AlumnoForm(request.POST)
        if formularioActualizarAlumno.is_valid():
            cedula = formularioActualizarAlumno.cleaned_data.get('cedula')
            nombres = formularioActualizarAlumno.cleaned_data.get('nombres')
            apellido_materno = formularioActualizarAlumno.cleaned_data.get('apellido_materno')
            apellido_paterno = formularioActualizarAlumno.cleaned_data.get('apellido_paterno')
            fecha_nacimiento = formularioActualizarAlumno.cleaned_data.get('fecha_nacimiento')
            sexo = formularioActualizarAlumno.cleaned_data.get('sexo')
            instance.cedula = cedula
            instance.nombres = nombres
            instance.apellido_paterno = apellido_paterno
            instance.apellido_materno = apellido_materno
            instance.fecha_nacimiento = fecha_nacimiento
            instance.sexo = sexo
            instance.save()
        return HttpResponseRedirect(reverse('listarAlumnos'))
    else:
        formularioActualizarAlumno = forms.AlumnoForm(instance=instance)
        return render(request, "formularioActualizarAlumno.html",{'formularioActualizarAlumno':formularioActualizarAlumno})

def eliminarAlumno(request, id):
    alumno = models.Alumno.objects.get(id=id)
    alumno.delete()
    return HttpResponseRedirect(reverse('listarAlumnos'))

